from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import Files, User, Profile

admin.site.register(Files)
admin.site.register(User,UserAdmin)
admin.site.register(Profile)

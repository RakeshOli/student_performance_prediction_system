from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings
from ckeditor.fields import RichTextField



class User(AbstractUser):
    pass



class Profile(models.Model):
    TEACHER ="TEA"
    STUDENT="STU"
    USER_CHOICES = [
        (TEACHER, 'TEACHER'),
        (STUDENT, 'STUDENT'),
    ]
    type = models.CharField(
        max_length=100,
        choices=USER_CHOICES,
        default=TEACHER,
    )
    # parent_id=models.IntegerField(null=True,blank=True)
    # is_active=models.BooleanField(default=True)
    profile_photo=models.FileField()
    user=models.OneToOneField(settings.AUTH_USER_MODEL,related_name='profile_user',on_delete=models.CASCADE)

    # def get_absolute_url_update(self):
    #     return reverse("client_portal:member-update", kwargs={"pk": self.pk})
    # def get_absolute_url_delete(self):
    #     return reverse("client_portal:member-delete", kwargs={"pk": self.pk})
    def __str__(self):
        return self.user.username

    # def get_absolute_url_update(self):
    #     return reverse("client_portal:member-permissions-list-edit",kwargs={"pk": self.profile.pk})
    def get_absolute_url_update(self):
        return reverse("client_portal:member-update", kwargs={"pk": self.user.pk})
    def get_absolute_url_delete(self):
        return reverse("client_portal:member-delete", kwargs={"pk": self.user.pk})



class Files(models.Model):
    name = models.CharField(max_length=300, null=True)
    file = models.FileField()
    description = RichTextField(blank=True,null=True)

    def __str__(self):
        return self.name

    def get_absolute_url_create(self):
        return reverse("files:file-create", kwargs={"pk": self.pk})

    def get_absolute_url_detail(self):
        return reverse("files:file-detail", kwargs={"pk": self.pk})

    def get_absolute_url_update(self):
        return reverse("files:file-update", kwargs={"pk": self.pk})

    def get_absolute_url_delete(self):
        return reverse("files:file-delete", kwargs={"pk": self.pk})

# import numpy as np
# import pandas as pd
# from time import time
# from sklearn.metrics import f1_score
# from sklearn.naive_bayes import GaussianNB
# from sklearn import metrics
# from sklearn.model_selection import train_test_split
#
# # Read student data
# student_data = pd.read_csv(r'C:\Users\hp\Desktop\percent.csv')
# student_data_1 = pd.read_csv(r'C:\Users\hp\Desktop\precent1.csv')
# final = pd.read_csv(r'C:\Users\hp\Desktop\final.csv')
# print("Student data read successfully!")
#
# n_students = len(student_data.index)
#
#
# n_features = len(student_data.columns)
#
#
#
# # Print the results
# print("Total number of students: {}".format(n_students))
# print("Number of features: {}".format(n_features))
#


#
# # Extract feature columns
# feature_cols = list(student_data.columns[:-1])
#
# # Extract target column 'passed'
# target_col = student_data.columns[-1]
#
# # Show the list of columns
# print("Feature columns:\n{}".format(feature_cols))
# print("\nTarget column: {}".format(target_col))
#
# # Separate the data into feature data and target data (X_all and y_all, respectively)
# X_all = student_data[feature_cols]
# y_all = student_data[target_col]
# X_all_1 = student_data_1[feature_cols]
#
# # Show the feature information by printing the first five rows
# print("\nFeature values:")
# print(X_all.head())
#
# def preprocess_features(X):
#
#     output = pd.DataFrame(index=X.index)
#
#
#     for col, col_data in X.iteritems():
#
#
#         if col_data.dtype == object:
#
#             col_data = pd.get_dummies(col_data, prefix=col)
#
#
#         output = output.join(col_data)
#
#     return output
#
# X_all = preprocess_features(X_all)
# X_all_1 = preprocess_features(X_all_1)
# print("Processed feature columns ({} total features):\n{}".format(len(X_all.columns), list(X_all.columns)))
#
#
# from sklearn.model_selection import train_test_split
#
#
#
#
# # X_train, X_test, y_train, y_test = train_test_split(X_all, y_all, test_size=num_test, random_state=42)
# X_train = student_data.drop('sem7', axis=1)
# y_train = student_data.loc[:, 'sem7']
# X_test = student_data_1.drop('sem7',axis=1)
# y_test = student_data_1.loc[:, 'sem7']
#
# print("Training set has {} samples.".format(X_train.shape[0]))
# print("Testing set has {} samples.".format(X_test.shape[0]))
# print(X_train)
# print(X_test)
#
#
#
# model = GaussianNB()
# model.fit(X_train, y_train)
#
# y_pred = model.predict(X_test)
# # y_pred_1 = np.concatenate(y_pred)
# # y_pred_1 = np.array(y_pred)
# print("predicted result: ", y_pred)
#
# print("Accuracy:", metrics.accuracy_score(y_test, y_pred))


import numpy as np
import pandas as pd
from time import time
from sklearn.metrics import f1_score
from sklearn.naive_bayes import GaussianNB
from sklearn import metrics
from sklearn.model_selection import train_test_split


student_data = pd.read_csv(r'C:\Users\hp\Downloads\percent.csv')
student_data_1 = pd.read_csv(r'C:\Users\hp\Desktop\precent1.csv')
final = pd.read_csv(r'C:\Users\hp\Desktop\stu_percent.csv')
print("Student data read successfully!")

n_students = len(student_data.index)

n_features = len(student_data.columns) - 1

# Print the results
print("Total number of students: {}".format(n_students))
print("Number of features: {}".format(n_features))

# Extract feature columns
feature_cols = list(student_data.columns[:-1])

# Extract target column 'passed'
target_col = student_data.columns[-1]


print("Feature columns:\n{}".format(feature_cols))
print("\nTarget column: {}".format(target_col))

# Separate the data into feature data and target data (X_all and y_all, respectively)
X_all = student_data[feature_cols]
y_all = student_data[target_col]
X_all_1 = student_data_1[feature_cols]

# Show the feature information by printing the first five rows
print("\nFeature values:")
print(X_all.head())


def preprocess_features(X):
    output = pd.DataFrame(index=X.index)

    for col, col_data in X.iteritems():

        if col_data.dtype == object:
            col_data = pd.get_dummies(col_data, prefix=col)

        output = output.join(col_data)

    return output


X_all = preprocess_features(X_all)
X_all_1 = preprocess_features(X_all_1)
print("Processed feature columns ({} total features):\n{}".format(len(X_all.columns), list(X_all.columns)))

from sklearn.model_selection import train_test_split

# X_train, X_test, y_train, y_test = train_test_split(X_all, y_all, test_size=num_test, random_state=42)
X_train = student_data.drop('sem7', axis=1)
y_train = student_data.loc[:, 'sem7']
X_test = student_data_1.drop('sem7', axis=1)
y_test = student_data_1.loc[:, 'sem7']

print("Training set has {} samples.".format(X_train.shape[0]))
print("Testing set has {} samples.".format(X_test.shape[0]))
print(X_train)
print(X_test)

model = GaussianNB()
model.fit(X_train, y_train)

y_pred = model.predict(X_test)
# y_pred_1 = np.concatenate(y_pred)
# y_pred_1 = np.array(y_pred)
print("predicted result: ", y_pred)

print("Accuracy:", metrics.accuracy_score(y_test, y_pred))

final['pred_percentage'] = y_pred
final.to_csv(r'C:\Users\hp\Desktop\stu_percent.csv', index=False)






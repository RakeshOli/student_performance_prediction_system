from django.urls import path

from .views import FileCreateView,FileDetailView,FileDeleteView,FileListView, LoginView,DashboardView, signout , SearchView, Home





app_name = 'performance'
urlpatterns = [

    path('', Home.as_view(), name='home-view'),
    path('login/', LoginView.as_view(), name='login-view'),
    path('dashboard/', DashboardView.as_view(), name='dashboard-view'),

    path('logout/',signout,name='logout'),


    path('file-create/',FileCreateView.as_view(),name='file-create'),
    path('file-list/',FileListView.as_view(),name='file-list'),
    path('file-detail/<int:pk>/',FileDetailView.as_view(),name='file-detail'),
    #path('detail/file-update/<int:pk>/',FileUpdate.as_view(),name='file-update'),
    path('file-delete/<int:pk>/',FileDeleteView.as_view(),name='file-delete'),

    path('search/<int:pk>',SearchView.as_view(),name='file-search'),

]
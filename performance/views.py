from django.shortcuts import render,reverse,redirect,get_object_or_404
from django.urls import reverse_lazy
from performance.models import Files,User,Profile
from performance.forms import FileForm, LoginForm, RegisterForm, SearchForm
from django.views.generic.edit import FormView

from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView, DeleteView
from django.views.generic import ListView, TemplateView, View

from django.contrib.auth import login, logout
from django.contrib.auth import authenticate
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.hashers import make_password

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.contrib import messages


from django.db.models import Q

import pandas as pd
import csv


import numpy as np


from time import time
from sklearn.metrics import f1_score
from sklearn.naive_bayes import GaussianNB
from sklearn import metrics



class LoginView(FormView):
    content = {}
    content['form'] = LoginForm
    # member=Member.objects.all()
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(LoginView, self).dispatch(request, *args, **kwargs)

    def get(self, request):
        content = {}
        content['form'] = LoginForm
        return render(request, 'login/login.html', content)

    def post(self, request, **kwargs):
        form = LoginForm(request.POST, request.FILES or None)
        if form.is_valid():
            email=form.cleaned_data['email']
            password=form.cleaned_data['password']
            users = User.objects.filter(email=email)

            if users:
                user = authenticate(username=users.first().username, password=password)
                if user:
                    profile = Profile.objects.filter(user=user)

                    types = profile.filter(Q(type='TEA'))
                    types1 = profile.filter(Q(type='STU'))
                    user_id = request.user.id
                    if types:
                        login(request, user)
                        return redirect('performance:dashboard-view')

                    else:
                        login(request, user)
                        return redirect('performance:file-search')

                    # else:
                    #     login(request, user)
                    #     form = RequestSetupForm
                    #     return render(request, 'requestsetup/requestsetup-create1.html', {'profiles': profiles, 'types': types,'form':form})
                    # else:
                    #     login(request,user)
                    #     msg = "You are not active"
                    #     messages.success(request, msg, fail_silently=True)
                    #     return redirect('performance:login-view')
                else:
                    # login(request,user)
                    msg = "Your password is incorrect"
                    messages.success(request, msg, fail_silently=True)
                    return redirect('performance:login-view')
            else:
                # login(request,user)
                msg = "Your email is incorrect"
                messages.success(request, msg, fail_silently=True)
                return redirect('performance:login-view')

        content = {}
        content['form'] = form
        return render(request, 'login/login.html', content)






def signout(request):
    logout(request)
    print('logged out')

    return redirect(reverse('performance:login-view'))


class DashboardView(LoginRequiredMixin, FormView):
    login_url = ('performance:login-view')

    def get(self, request):
        files=Files.objects.all()

        return render(request, 'login/dashboard.html', {'files':files})







class FileCreateView(FormView):
    content = {}
    content['form'] = FileForm
    model = Files
    form_class = FileForm
    template_name = 'files/file-create.html'

    def get(self, request):






        return render(request, 'files/file-create.html', self.content)


    def post(self, request):
        form = FileForm(request.POST, request.FILES)
        print(form)

        if form.is_valid():

            save_it = form.save(commit=False)
            save_it.save()
        return redirect(reverse('performance:file-list'))

#
#
#
class FileListView(ListView):
    model = Files

    template_name = 'files/file-list.html'
    context_object_name = 'file'

    def get_queryset(self):
        file = Files.objects.all()
        return file

    def post(self, request, *args, **kwargs):
        files = request.POST.get('list')
        print(files)
        test_file = Files.objects.get(id=files)
        print("rakesh", test_file)



        return render(request, 'files/file-list.html')
        # student_value = request.POST.get('student_data_1')






class FileDetailView(View):
    model = Files
    template_name = 'files/file-detail.html'

    def get(self,request,*args,**kwargs):
        file = Files.objects.get(id=self.kwargs['pk'])
        student_data = pd.read_csv(r'C:\Users\hp\Desktop\student1.csv')

        # student_data_html = student_data.to_html()
        # context = {'loaded_data': student_data_html}
        # return render(request, "files/file-detail.html", context)
        return render(request, 'files/file-detail.html', {'columns': student_data.columns, 'rows': student_data.to_dict('records')})
        return render(request, self.template_name, {'file':file})
# #
#
class FileDeleteView(DeleteView):
    template_name = 'files/file-delete.html'

    model = Files
    success_url = reverse_lazy('performance:file-delete')




class SearchView(DetailView):
    template_name = 'files/search.html'

    model = Files

    def get(self, request,  *args, **kwargs):

        # files = request.POST.get('list')
        # print(files)
        test_file = Files.objects.get(id=3)

        print("rakesholidon", test_file)

        path1 = test_file.file.path
        print('oli',path1)





        import numpy as np
        import pandas as pd
        import os
        from time import time
        from sklearn.metrics import f1_score
        from sklearn.naive_bayes import GaussianNB
        from sklearn import metrics
        from sklearn.model_selection import train_test_split



        # path = os.getcwd()
        # path1 = path + test_file
        # print(path1)

        # from pathlib import Path
        # import os
        #
        # parent = Path(__file__).parent
        # car_pic = os.path.join(parent,test_file)
        # print("dang", car_pic)



        # Read student data
        student_data = pd.read_csv(path1)
        student_data_1 = pd.read_csv(r'C:\Users\hp\Desktop\precent1.csv')
        final = pd.read_csv(r'C:\Users\hp\Desktop\stu_percent.csv')
        print("Student data read successfully!")

        n_students = len(student_data.index)


        n_features = len(student_data.columns) - 1





        # Print the results
        print("Total number of students: {}".format(n_students))
        print("Number of features: {}".format(n_features))


        # Extract feature columns
        feature_cols = list(student_data.columns[:-1])

        # Extract target column 'passed'
        target_col = student_data.columns[-1]

        # Show the list of columns
        print("Feature columns:\n{}".format(feature_cols))
        print("\nTarget column: {}".format(target_col))

        # Separate the data into feature data and target data (X_all and y_all, respectively)
        X_all = student_data[feature_cols]
        y_all = student_data[target_col]
        X_all_1 = student_data_1[feature_cols]

        # Show the feature information by printing the first five rows
        print("\nFeature values:")
        print(X_all.head())

        def preprocess_features(X):

            output = pd.DataFrame(index=X.index)


            for col, col_data in X.iteritems():


                if col_data.dtype == object:

                    col_data = pd.get_dummies(col_data, prefix=col)


                output = output.join(col_data)

            return output

        X_all = preprocess_features(X_all)
        X_all_1 = preprocess_features(X_all_1)
        print("Processed feature columns ({} total features):\n{}".format(len(X_all.columns), list(X_all.columns)))


        from sklearn.model_selection import train_test_split




        # X_train, X_test, y_train, y_test = train_test_split(X_all, y_all, test_size=num_test, random_state=42)
        X_train = student_data.drop('sem7', axis=1)
        y_train = student_data.loc[:, 'sem7']
        X_test = student_data_1.drop('sem7', axis=1)
        y_test = student_data_1.loc[:, 'sem7']

        print("Training set has {} samples.".format(X_train.shape[0]))
        print("Testing set has {} samples.".format(X_test.shape[0]))
        print(X_train)
        print(X_test)



        model = GaussianNB()
        model.fit(X_train, y_train)

        y_pred = model.predict(X_test)
        # y_pred_1 = np.concatenate(y_pred)
        # y_pred_1 = np.array(y_pred)
        print("predicted result: ", y_pred)

        print("Accuracy:", metrics.accuracy_score(y_test, y_pred))

        final['pred_percentage'] = y_pred
        final.to_csv(r'C:\Users\hp\Desktop\stu_percent.csv', index=False)



        return render(request, 'files/search.html', {'final':final})

    def post(self, request, *args, **kwargs):
        values = request.POST.get('symbol')
        # student_value = request.POST.get('student_data_1')
        print(values)

        # files = request.POST.get('list')
        # print(files)
        # test_file = Files.objects.get(id = files)
        # print("rakesh", test_file)

        final = pd.read_csv(r'C:\Users\hp\Desktop\final.csv')

        sym = final['symbol']






        stu_percent = pd.read_csv(r'C:\Users\hp\Desktop\stu_percent.csv')





        dk1 = final.query('semester == 1 and symbol == @values')

        dk2 = final.query('semester == 2 and symbol == @values')
        dk3 = final.query('semester == 3 and symbol == @values')
        dk4 = final.query('semester == 4 and symbol == @values')
        dk5 = final.query('semester == 5 and symbol == @values')
        dk6 = final.query('semester == 6 and symbol == @values')
        dk7 = stu_percent.query('symbol == @values')

        return render(request, 'files/search.html',{'columns':dk1.columns, 'rows':dk1.to_dict('records'), 'columns1':dk2.columns, 'rows1':dk2.to_dict('records'),  'columns2':dk3.columns, 'rows2':dk3.to_dict('records'), 'columns3':dk4.columns, 'rows3':dk4.to_dict('records'), 'columns4':dk5.columns, 'rows4':dk5.to_dict('records'), 'columns5':dk6.columns, 'rows5':dk6.to_dict('records'),'columns':dk7.columns, 'rows':dk7.to_dict('records') ,'values':values, 'sym':sym})





class Home(TemplateView):

    template_name = 'home.html'









